<?php 

namespace App\Services;

use App\Traits\ConsumesExternalService;

class AuthorService
{
	use ConsumesExternalService;

	/**
     * The base uri to be used to consume the authors service
     * @var string
     */
	public $baseUri;

	public function __construct()
	{
		$this->baseUri = config('services.authors.base_uri');
	}

	 /**
     * Get the full list of authors from the authors service
     * @return string
     */
	public function obtainAuthors()
	{
		return $this->performRequest('GET','/authors');
	}

	public function createAuthor($data)
    {
        return $this->performRequest('POST', '/authors', $data);
    }

    public function obtainAuthor($author)
	{
		return $this->performRequest('GET',"/authors/{$author}");
	}

	public function editAuthor($data, $author)
	{
		return $this->performRequest('PUT',"/authors/{$author}", $data);
	}

	public function deleteAuthor($author)
	{
		return $this->performRequest('DELETE',"/authors/{$author}");
	}
}