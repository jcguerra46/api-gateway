<?php 

namespace App\Services;

use App\Traits\ConsumesExternalService;

class BookService
{
	use ConsumesExternalService;

	/**
     * The base uri to be used to consume the authors service
     * @var string
     */
	public $baseUri;

	public function __construct()
	{
		$this->baseUri = config('services.books.base_uri');
	}

	 /**
     * Get the full list of books from the books service
     * @return string
     */
	public function obtainBooks()
	{
		return $this->performRequest('GET','/books');
	}

	public function createBook($data)
	{
		return $this->performRequest('POST', '/books', $data);
	}

	public function obtainBook($book)
	{
		return $this->performRequest('GET', "/books/{$book}");
	}

	public function editBook($data, $book)
	{
		return $this->performRequest('PUT', "/books/{$book}", $data);
	}

	public function deleteBook($book)
	{
		return $this->performRequest('DELETE', "/books/{$book}");
	}
}